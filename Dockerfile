FROM python:3.8-alpine

ENV IS_DOCKER True

ADD app /app
RUN chmod 755 /app/ddb
RUN chmod 755 /app/entrypoint

RUN apk add bash
RUN apk add --no-cache --virtual build-deps gcc musl-dev g++
RUN pip install --upgrade pip setuptools wheel
RUN python -m pip install -r /app/requirements.txt --target /app
RUN pip list
#RUN apk del .build-deps gcc musl-dev

ENTRYPOINT ["/bin/bash", "/app/entrypoint"]
