IMAGE_NAME="ddb"
VERSION="latest"

default: docker

docker:
	docker build --rm --tag $(IMAGE_NAME):$(VERSION) .
