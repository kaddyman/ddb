# ddb

python script to control listing, loading and deleting items in DynamoDB tables. This script 
can be used on any DynamoDB table even if it has binary content (list only not load).


When running this as a docker container make sure the json file is in /app and that's the only 
path that is checked. When running this as a stand alone script your json file path can be 
whatever you want.

---

### python script location

You can run the python scirpt on it's own from here. `/app/ddb`


Or you can build the docker container and run it with the bash script ddb. 
You can buld the docker image with `make` and the commands are the same. 

---

### sample command

dump table items in json  
`AWS_PROFILE=ken ./ddb --table sftp-table`


delete all items in table that match the primary key  
`AWS_PROFILE=ken ./ddb --table sftp-table --key sftp_home_folder --delete`


load the json file into the table  
`AWS_PROFILE=ken ./ddb --table sftp-table --json loader.json`


delete all items that match the primary key and load the json file into the table  
`AWS_PROFILE=ken ./ddb --table sftp-table --key sftp_home_folder --delete --json loader.json`


delete one item that maches the key/value in the table  
`AWS_PROFILE=ken ./ddb --table sftp-table --key sftp_home_folder --value devops --deleteitem`
